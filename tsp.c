/*
    Christian Vuerich
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include "tsp.h"
#include <time.h>

 
clock_t start;

int main (int argc, char **argv)
{ 
    if (argc == 4)
    {
        int min, mincost = -1;
        double err;
        char *input = argv[1];
        graph *g = parse_TSP (input);
        srand(time(NULL));
        for (int i = 0 ; i < atoi(argv[2]); i++)
        {
            printf("%d\n", i);

            start = clock ();

            
            int sed = rand();
            srand (sed);
            sed = rand();
            srand (sed);

            int ran = (rand() % g->size) + 1;
            solution *s = nearest_neighbor (g, ran);

            if (is_valid_solution (s, g))
            {  
                compute_cost (s, g->costMap);
                iterated_local_search (s, g, atof(argv[3]), 0.95);
                if (is_valid_solution (s, g))
                {
                    // printf ("best_cost = %d\n", g->best_known); 
                    // printf ("cost = %d\n", s->cost);
                    // printf("\n"); 
                    // printf("%f\n", 100.0 / g->best_known * (s->cost - g->best_known));
                    // printf("\n"); 

                    if(mincost == -1 || mincost > s->cost)
                    {
                        mincost = s->cost;
                        min = sed;
                        err = 100.0 / g->best_known * (s->cost - g->best_known);
                    }
                }

                // FILE *file  = fopen ("test",  "w");

                // if (file != 0)
                // {
                //     printf("Writing solution to file\n");

                //     for (int i = 0; i < g->size; i++)
                //     {
                //         fprintf (file, "%d ", s->path[i] );
                //     }
                // }

                // fclose (file);
                // printf("Creating immage\n");
                // system ("rm -rf output.png");
                // system ("./main -answer='test' -problem='mock.tsp'");
                // printf("Deleting solution file\n");
                // system ("rm -rf test");


            }

            else
                perror ("Not valid solution\n");

            
            free (s);
            s = NULL;

            //return 0;
        }
        g = NULL;
        free (g);

        printf("best seed = %d, error %f, cost %d\n", min, err, mincost);
        
        system ("afplay /System/Library/Sounds/Glass.aiff");

        return 0;
    }

    else
      return -1;
}


graph* parse_TSP (char *filepath)
{  
    FILE *input_file;
    node *n = NULL;
    input_file = fopen (filepath, "r");

    graph *g = (graph*) malloc (sizeof (graph));

    if (input_file == 0)
    {
        perror("Canot open input file\n");
        exit(-1);
    }

    else
    {
        size_t linecap = 0;
        ssize_t linelen;
        char *line = NULL;
        bool store = false;
        int value;
        double x;
        double y;
        char *end;
        int size = -1;

        while ((linelen = getline (&line, &linecap, input_file))  >  0)
        {
            if (line[linelen-1] == '\n')
            {
                line[linelen-1] = '\0';
                linelen--;
            }

            char *token, *string;
            string = line;
            token = strsep (&string," \t\n");

            if (token == NULL || strlen(token) == 0) continue;

            if (!store)
            {
                if (0 == strcmp(token,"DIMENSION:"))
                {
                    token = strsep (&string, " ");
                    size = atoi (token);
                    n = (node*) malloc (size * sizeof (node));
                }

                if (0 == strcmp(token,"BEST_KNOWN:"))
                {
                    token = strsep (&string, " ");
                    g->best_known = atoi (token);
                }

                if (0 == strcmp (token,"NODE_COORD_SECTION"))
                {
                    store = true;
                }

            }

            else
            {
                if (size == -1)
                    perror ("Canot read DIMENSION\n");

                if (0 == strcmp (token,"EOF"))
                {
                    store = false;
                }

                else
                {
                    value = atoi (token);
                    token = strsep (&string, " ");
                    vector2D *coordinates = (vector2D*) malloc (sizeof (vector2D));
                    x = strtod (token, &end);
                    token = strsep (&string, " ");
                    y = strtod (token, &end);
                    coordinates->x = x;
                    coordinates->y =y;
                    n->coordinates = *coordinates;
                    n->id = value;
                    n++;
                }
            }
        }

        for (int i = 0; i < size; i++)
        {
            n--;
        }

        g->size = size;
        g->nodes = n;
        generate_cost_matrix (g);
    }

    fclose (input_file);

    return g;
}


void print_all (solution *s)
{
    for (int i=0; i  <  s->graphSize; i++)
    {
        printf ("%d ", s->path[i]);
    }
    printf ("\n");
}


int is_in_once (int id, int *array, int size)
{
    int appearence = 0;

    for (int i = 0; i < size; i++)
    {
        if (id == array[i])
            appearence++;

        // if (appearence > 1)
        //     return false;
    }

    // if (appearence == 0)
    //     return false;

    // return true;
    return appearence;
}


bool is_valid_solution (solution *s, graph *g)
{
    node *n = g->nodes;
    int size = g->size;

    if (s->graphSize == size)
    {
        for (int i=0; i < size; i++)
        {
            // if (is_in_once(n[i].id, s->path, size) == false)
            //     return false;
            if (is_in_once(n[i].id, s->path, size) != 1)
            {
                system ("afplay /System/Library/Sounds/Basso.aiff");
                printf("ERROR node %d, appears %d times\n", n[i].id, is_in_once(n[i].id, s->path, size));
                return false;
            }
        }

        return true;
    }

    else
        return false;
}


void compute_cost (solution *solution, double **costMap)
{
    double cost = 0;

    for (int i = 0; i  <  solution->graphSize - 1; i++)
    {
        cost += costMap[solution->path[i] - 1][solution->path[i + 1] - 1];
    }

    cost += costMap[solution->path[solution->graphSize - 1] - 1][solution->path[0] - 1];

    solution->cost = (int)cost;
}


void generate_cost_matrix (graph *g)
{
    node *n = g->nodes;
    int size = g->size;
    double **costmap = (double**) malloc (size * sizeof (double*));

    for (int i = 0; i < size; ++i)
    {
        costmap[i] = (double*) malloc (size * sizeof (double));
    }

    for (int i=0; i < size; i++)
    {
        for (int y=0; y < size; y++)
        {
            costmap[i][y]=  sqrt ((n[i].coordinates.x - n[y].coordinates.x) * (n[i].coordinates.x - n[y].coordinates.x) + 
                            (n[i].coordinates.y - n[y].coordinates.y) * (n[i].coordinates.y - n[y].coordinates.y));
        }
    }

    g->costMap = costmap;
}


void invert (int i, int j, solution *s)
{
    if (i == j)
        return;

    int tmp;

    int index_i = get_index (i, s);

    int index_j = get_index (j,s);

    int index = get_number_of_element(index_i, index_j, s) / 2;
        
    for (int t = 0; t < index; t++)
    {
        if (index_i  >  s->graphSize-1)
            index_i = 0;

        if (index_j < 0)
            index_j = s->graphSize-1;

        tmp = s->path[index_j];
        s->path[index_j] =  s->path[index_i];
        s->path[index_i] = tmp;
        index_j -= 1;
        index_i += 1;
    }
}


int get_index (int i, solution *s)
{
    for (int j = 0; j  <  s->graphSize; j++)
    {
        if (s->path[j] == i)
            return j;
    }

    perror ("Not valid node id\n");
    exit (-1);
}


solution* nearest_neighbor (graph *g, int start_node)
{   
    solution * s = (solution*) malloc (sizeof (solution));
    s->path = (int*) malloc (sizeof (int) * g->size);
    s->graphSize = g->size;

    s->path[0] = start_node;

    for (int i = 1; i < g->size; i++)
    {
        double min = -1;
        int toadd = -1;

        for (int y = 0; y < g->size; y++)
        {
            if(!is_in_once (y + 1, s->path, i + 1))
            {
                if(min == -1 || min > g->costMap [s->path[i - 1] - 1][y])
                {
                    min = g->costMap [s->path[i - 1] - 1][y];
                    toadd = y + 1;
                }
            }
        }

        s->path[i] = toadd;
    }

    return s;
}


void two_opt (solution *s, graph *g, bool first_improvement)
{
    int best_gain = 1;
    int gain;
    int best_i;
    int best_y;

    while (best_gain != 0)
    {
        best_gain = 0;
        best_i = 0;
        best_y = 0;

        for (int i = 0; i < s->graphSize - 1; i++)
        {
            for (int y = 0; y < s->graphSize - 1 ; y++)
            {
                if (i == y || (i + 1) % g->size == y || (y + 1) % g->size == i)
                    break;//Because at the we still see every cople. It doesnt matter in which order

                gain = two_opt_gain (i, y, g, s);

                if (gain < best_gain)
                {
                    best_gain = gain;
                    best_i = i;
                    best_y = y;

                    if (first_improvement)
                        break;
                }
            }

            if (best_gain < 0 && first_improvement)
                break;
        } 

        invert (s->path[(best_i + 1) % s->graphSize], s->path[best_y], s);
    }

    compute_cost (s, g->costMap);
}


int two_opt_gain (int i, int y, graph *g , solution *s)
{
    double gain = g->costMap [s->path[i] - 1][s->path[y] - 1] +
            g->costMap [s->path[(i + 1) % g->size] - 1][s->path[(y + 1) % g->size] - 1] -
            g->costMap [s->path[y] - 1][s->path[(y + 1) % g->size] - 1] -
            g->costMap [s->path[i] - 1][s->path[(i + 1) % g->size] - 1];

    return (int) gain;
}


solution* reconstruct_double_bridge (int i, int j, int x, int y, solution *s, graph *g)
{
    if (i == j || i == x || i == y || j == x || j == y || x == y ||
        (i + 1) % s->graphSize == j || (i + 1) % s->graphSize == x || (i + 1) % s->graphSize == y ||
        (j + 1) % s->graphSize == i || (j + 1) % s->graphSize == x || (j + 1) % s->graphSize == y ||
        (x + 1) % s->graphSize == i || (x + 1) % s->graphSize == j || (x + 1) % s->graphSize == y ||
        (y + 1) % s->graphSize == i || (y + 1) % s->graphSize == j || (y + 1) % s->graphSize == x)
        return NULL;

    solution *copy = copy_solution (s);
    int group1, group2, group3, group4;

    group1 = get_number_of_element ((x + 1), y, copy);
    group2 = get_number_of_element ((j + 1), x, copy);
    group3 = get_number_of_element ((i + 1), j, copy);
    group4 = get_number_of_element ((y + 1), i, copy);

    int *pos = (int*) malloc (sizeof (int));
    *pos = 0;

    copy_path_section (x, group1, pos, s, copy);
    copy_path_section (j, group2, pos, s, copy);
    copy_path_section (i, group3, pos, s, copy);
    copy_path_section (y, group4, pos, s, copy);

    compute_cost (copy, g->costMap);

    free (pos);
    pos = NULL;

    if (! is_valid_solution(copy, g))
        printf("ERROR!!! i = %d,  j = %d,  x = %d,  y = %d,  \n", i, j, x, y);

    return copy;
}


void copy_path_section (int start, int size, int *offset, solution *s, solution *copy)
{
    for (int index = (start + 1) % s->graphSize; index < ((start + 1) % s->graphSize) + size; index++)
    {
        copy->path[*offset] = s->path[index % s->graphSize];
        (*offset)++;
    }
}


int get_number_of_element (int a, int b, solution *s)
{
    a = a % s->graphSize;
    b = b % s->graphSize;

    if (a < b)
        return b - a + 1;

    else
        return (b + 1) + (s->graphSize - a);
}


void iterated_local_search (solution *s, graph *g, double temperature, double cooling_rate)
{
    printf("%f   %f\n", temperature, cooling_rate );

    int *indexes = (int*) malloc (sizeof (int) * 4);

    int t;

    int delta;

    double probability;

    solution *tmp;

    int iteration = 0;

    while (temperature > 0.00001) 
    {
        tmp = NULL;

        while (tmp == NULL)
        {
            indexes[0] = rand () % g->size;
            indexes[1] = rand () % g->size;
            indexes[2] = rand () % g->size;
            indexes[3] = rand () % g->size;

            qsort (indexes, 4, sizeof (int), compare_integers);

            tmp = reconstruct_double_bridge (indexes[0], indexes[1], indexes[2], indexes[3], s, g);
        }

        two_opt(tmp, g, false);

        delta = tmp->cost - s->cost;
        probability = (double)rand () / (double)(RAND_MAX);

        if( delta <= 0 || probability < exp (((double)((-1) * delta)) / ((double)temperature)))
            *s = *tmp;

        temperature *= cooling_rate;

        iteration++;

        t = (clock() - start) * 1000 / CLOCKS_PER_SEC;

        if ((t / 1000) >= 177)
        {
            printf("time limit reached\n");
            break;
        }
    }

    free (indexes);
    indexes = NULL;
}


solution* copy_solution (solution *s)
{
    solution *copy = (solution*) malloc (sizeof (solution));
    copy->graphSize = s->graphSize;
    copy->cost = s->cost;
    copy->path = (int*) malloc (sizeof (int) * s->graphSize);

    for (int i = 0; i < s->graphSize; i++)
    {
        copy->path[i] = s->path[i];
    }

    return copy;
}


int compare_integers (const void *a, const void *b)
{
    const int *da = (const int *) a;
    const int *db = (const int *) b;
 
    return *da > *db;
}


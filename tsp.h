/*
    Christian Vuerich
*/


typedef struct vector2D
{
    double x,y;
} vector2D;


typedef struct node
{
    int id;
    vector2D coordinates;
} node;


typedef struct graph
{
    node *nodes;
    int size;
    double **costMap;
    int best_known;
} graph;


typedef struct solution
{
    int *path;
    int cost;
    int graphSize;
} solution;


typedef struct couple
{
    int id;
    int cost;
    int count;
} couple;


typedef enum { false, true } bool;



graph* parse_TSP (char*);

void print_all (solution*);

int is_in_once (int id, int*, int size);

bool is_valid_solution (solution*, graph*);

void compute_cost (solution*, double**);

void generate_cost_matrix (graph*);

solution* nearest_neighbor (graph*, int);

void invert (int, int, solution*);

int get_index (int, solution*);

void two_opt (solution*, graph*, bool);

inline int two_opt_gain (int, int, graph*, solution*);

solution* reconstruct_double_bridge (int, int, int, int, solution*, graph*);

void copy_path_section (int, int, int*, solution*, solution*);

int get_number_of_element (int, int, solution*);

void iterated_local_search (solution*, graph*, double, double);

solution* copy_solution (solution*);

int compare_integers (const void*, const void*);
